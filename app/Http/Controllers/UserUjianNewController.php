<?php
 
namespace App\Http\Controllers;
 
use App\Models\SoalUser;
use App\Models\UjianUser;
use App\Models\SoalAngkaHilang;
use Illuminate\Http\Request;
use Auth;
use DB;
use DateTime;
use PDF;
use QrCode;

class UserUjianNewController extends Controller
{
    public function info($id)
    {
        $query           = DB::table('ujian_users')
        ->select('ujians.id as ujian_id', 'ujians.name as ujian_name', 'ujians.jumlah_soal','ujians.waktu_pengerjaan','ujians.status','ujians.jenis_soal','users.name', 'users.id as user_id','ujian_users.id', 'ujian_users.token','ujian_users.jawaban_benar','ujian_users.jawaban_salah','ujian_users.nilai')
        ->selectRaw('DATE_FORMAT(ujian_users.start_date, "%Y-%m-%d %H:%i:%s") as start_date')
        ->selectRaw('DATE_FORMAT(ujian_users.finish_date, "%Y-%m-%d %H:%i:%s") as finish_date')
        ->selectRaw('DATE_FORMAT(ujians.tgl_ujian, "%Y-%m-%d") as tgl_ujian')
        ->selectRaw('DATE_FORMAT(ujians.tgl_ujian, "%d-%m-%Y") as tgl_ujian_indo')

        ->join('ujians', 'ujian_users.ujian_id', '=', 'ujians.id')
        ->join('users', 'ujian_users.user_id', '=', 'users.id')

        ->where('ujian_users.id','=', $id)
        ->whereNull('ujian_users.nilai')
        ->first();
        
        
        if ($query === null) {
            return redirect('token')->with('success', 'Maaf, Ujian tidak tersedia');
        }
        
        $data['row']    = $query;
        return view('user.info', $data);
                
    }


    public function mulai($id)
    {

        $query           = DB::table('ujian_users')
        ->select('ujians.id as ujian_id', 'ujians.name as ujian_name', 'ujians.jumlah_soal', 'ujians.token','ujians.waktu_pengerjaan','ujians.status','users.name', 'users.id as user_id','ujian_users.id','ujian_users.jawaban_benar','ujian_users.jawaban_salah','ujian_users.nilai','ujian_users.kurang_waktu_pengerjaan')
        ->selectRaw('DATE_FORMAT(ujian_users.start_date, "%Y-%m-%d %H:%i:%s") as start_date')
        ->selectRaw('DATE_FORMAT(ujian_users.finish_date, "%Y-%m-%d %H:%i:%s") as finish_date')
        ->selectRaw('DATE_FORMAT(ujians.tgl_ujian, "%Y-%m-%d") as tgl_ujian')
        ->selectRaw('DATE_FORMAT(ujians.tgl_ujian, "%d-%m-%Y") as tgl_ujian_indo')

        ->join('ujians', 'ujian_users.ujian_id', '=', 'ujians.id')
        ->join('users', 'ujian_users.user_id', '=', 'users.id')

        ->where('ujian_users.id','=', $id)
        ->whereNull('ujian_users.nilai')
        ->first();

        if ($query === null) {
            return redirect('token')->with('success', 'Maaf, Ujian tidak tersedia.');
        }
        if (date('Y-m-d') != $query->tgl_ujian) {
            return redirect('token')->with('success', 'Ujian hanya bisa diakses pada Tanggal : '.$query->tgl_ujian_indo);
        }


        $updateStartDate    = UjianUser::find($id);
        $updateStartDate->start_date        = date('Y-m-d H:i:s');
        $updateStartDate->save(); 

        $queryTampilSoals           = DB::table('soal_users')
        ->select('soal_users.id as soal_users_id', 
        'soal_users.id_jawaban', 
        'soal_users.benar_salah',
        'soal_choices.soal_choice',
        'soal_choices.id as id_soal')
        ->join('soal_choices', 'soal_users.id_soal', '=', 'soal_choices.id')
        ->where('soal_users.id_ujian_user','=', $id)
        ->orderBy('soal_users.id','asc')
        ->get();


        $soalHtml = "";
            $soalHtml .= '
            
            <div class="row " >
                
                
                
            ';

            $ii = 1;
            $tampilPaging  =    "";
            foreach($queryTampilSoals as $queryTampilSoal){
                if($ii ==1){

                    $tampilSoal = "";
                }
                else{
                    $tampilSoal = "none";
                }

                $soalHtml .= '
                <div class="col-12" id="soalNomor_'.$ii.'" style="display:'.$tampilSoal.'">
                    <b>'.$queryTampilSoal->soal_choice.'</b>
                ';

                $cekJawabanUser          = DB::table('soal_users')
                ->select('id_jawaban')
                ->where('id_ujian_user','=', $id)
                ->where('id_soal','=', $queryTampilSoal->id_soal)
                ->first();

                $queryTampilJawabans           = DB::table('soal_choice_jawabans')
                ->where('id_soal_choice','=', $queryTampilSoal->id_soal)
                ->get();
                    foreach($queryTampilJawabans as $queryTampilJawaban){

                        

                        if(!$cekJawabanUser){
                            $idJawabanDariUser  =   0;
                        }
                        else{
                            $idJawabanDariUser  =   $cekJawabanUser->id_jawaban;
                        }

                        if($idJawabanDariUser ==  $queryTampilJawaban->id){

                            $checked = "checked";
                        }
                        else{
                            $checked = "";
                        }

                        $soalHtml .= '
                        <div class="form-check">
                            <input class="form-check-input" type="radio" '. $checked .' name="jawaban_'.$queryTampilSoal->id_soal.'" id="flexjawaban_'.$queryTampilSoal->id_soal.'_'.$queryTampilJawaban->id.'" value="'.$queryTampilJawaban->id.'" onclick="jawabSoalNomor('.$ii.','. $queryTampilSoal->soal_users_id.','. $queryTampilJawaban->id.')">
                            <label class="form-check-label" for="flexjawaban_'.$queryTampilSoal->id_soal.'_'.$queryTampilJawaban->id.'">
                            '. $queryTampilJawaban->jawaban.'
                            </label>
                        </div>
                        ';
                    }
                $soalHtml .= '
                </div>
                ';

                    if($cekJawabanUser->id_jawaban == ''){
                        $activePaging  =   "";
                    }
                    else{
                        $activePaging  =   "active";
                    }

                    $tampilPaging .= '
                    <li id="nomorSoalBawah_'.$ii.'" class="page-item '.$activePaging.'"><a class="page-link" href="#" onclick="tampilSoalFromPaging('.$ii.')">'.$ii.'</a></li>';

            $ii++;
            }

            $soalHtml .= '
                <input id="inputSoalNomor" type="hidden" value="1">
            
            </div>
            ';
        
        
        
        
        $data['tampilSoals']    = $soalHtml;
        $data['tampilPaging']    = $tampilPaging;
        $data['row']            = $query;
        return view('user.mulai', $data);
        
    }

    function moveElement($array) {


        $a = rand(1, 4);
        $b = rand(1, 4);

        $p1 = array_splice($array, $a, 2);
        $p2 = array_splice($array, 4, $b);
        $array = array_merge($p2,$p1,$array);

        return $array;
    }

    function moveElementSoal($array) {


        $a = rand(1, 3);
        $b = rand(1, 3);

        $p1 = array_splice($array, $a, 2);
        $p2 = array_splice($array,3, $b);
        $array = array_merge($p2,$p1,$array);

        return $array;
    }

    function huruf($hurf) {


        switch ($hurf) {
            case "1":
              $HuruF = "A";
              break;
            case "2":
                $HuruF = "B";
              break;
            case "3":
                $HuruF = "C";
              break;
            case "4":
                $HuruF = "D";
                break;
            case "5":
                $HuruF = "E";
                break;
            default:
                $HuruF = "...";
        }

        return $HuruF;
    }



    public function submitJawaban(Request $request)
    {
        // $id = ujian_user.id
        $queryUjianBySoalUser          = DB::table('ujian_users')
        ->select('ujian_users.id','soal_users.id_soal')
        ->join('soal_users', 'ujian_users.id', '=', 'soal_users.id_ujian_user')
        ->where('soal_users.id','=', $request->soal_user_id)
        ->first();
        //dd($queryUjianBySoalUser);

        $queryUjian          = DB::table('ujian_users')
        ->select('ujians.id as ujian_id', 'ujians.name as ujian_name', 'ujians.jumlah_soal','ujians.waktu_pengerjaan','ujians.status','ujians.jenis_soal','users.name', 'users.id as user_id','ujian_users.id', 'ujian_users.token','ujian_users.jawaban_benar','ujian_users.jawaban_salah','ujian_users.nilai','ujian_users.kurang_waktu_pengerjaan')
        ->selectRaw('DATE_FORMAT(ujian_users.start_date, "%Y-%m-%d %H:%i:%s") as start_date')
        ->selectRaw('DATE_FORMAT(ujian_users.finish_date, "%Y-%m-%d %H:%i:%s") as finish_date')
        ->selectRaw('DATE_FORMAT(ujians.tgl_ujian, "%Y-%m-%d") as tgl_ujian')

        ->join('ujians', 'ujian_users.ujian_id', '=', 'ujians.id')
        ->join('users', 'ujian_users.user_id', '=', 'users.id')

        ->where('ujian_users.id','=', $queryUjianBySoalUser->id)
        ->whereNull('ujian_users.nilai')
        ->first();

        //dd($queryUjian);

        if (!$queryUjian) {
            return response()->json([
                'message' => 'ujian tidak tersedia',
                'status' => 'error',
            ]);
        }
        else{
            $queryJawabanBenarSalah          = DB::table('soal_choice_jawabans')
            ->select('status_jawaban')
            ->where('id_soal_choice','=', $queryUjianBySoalUser->id_soal)
            ->where('id','=', $request->id_jawaban)
            ->first();

            DB::table('soal_users')->where('id', $request->soal_user_id)->update([
                'id_jawaban'    => $request->id_jawaban,
                'benar_salah'   => $queryJawabanBenarSalah->status_jawaban,
                'updated_at'     => date('Y-m-d H:i:s')
            ]);

            $waktu_awal     =   strtotime($queryUjian->start_date);
            $waktu_akhir    =   strtotime(now()); // bisa juga waktu sekarang now()
            
            $start_date = new DateTime($queryUjian->start_date);
            $since_start = $start_date->diff(new DateTime(now()));
            $minutes = $since_start->days * 24 * 60;
            $minutes += $since_start->h * 60;
            $minutes += $since_start->i;
            $sisaMenit = $queryUjian->kurang_waktu_pengerjaan - $minutes; 
            //dd($sisaMenit);
            DB::table('ujian_users')->where('id', $queryUjianBySoalUser->id)->update([
                'kurang_waktu_pengerjaan'    => $sisaMenit
            ]);


            return response()->json([
                'status' => 'success',
            ]);
        }

    }

    public function selesai($id)
    {
        // $id = ujian_user.id

        $query           = DB::table('ujian_users')
        ->select('ujians.id as ujian_id', 'ujians.name as ujian_name', 'ujians.jumlah_soal','ujians.waktu_pengerjaan','ujians.status','users.name', 'users.id as user_id','ujian_users.id', 'ujian_users.token','ujian_users.jawaban_benar','ujian_users.jawaban_salah','ujian_users.nilai')
        ->selectRaw('DATE_FORMAT(ujian_users.start_date, "%Y-%m-%d %H:%i:%s") as start_date')
        ->selectRaw('DATE_FORMAT(ujian_users.finish_date, "%Y-%m-%d %H:%i:%s") as finish_date')
        ->selectRaw('DATE_FORMAT(ujians.tgl_ujian, "%Y-%m-%d") as tgl_ujian')

        ->join('ujians', 'ujian_users.ujian_id', '=', 'ujians.id')
        ->join('users', 'ujian_users.user_id', '=', 'users.id')

        ->where('ujian_users.id','=', $id)
        ->whereNotNull('ujian_users.nilai')
        ->first();

        if ($query === null) {
            return redirect('user')->with('success', 'Ujian tidak tersedia');
        }

        
        $data['row']    = $query;

        return view('user.selesai', $data);
    }

   


    public function submitUjian(Request $request, $id)
    {
        

        $query           = DB::table('ujian_users')
        ->select('ujians.id as ujian_id', 'ujians.name', 'ujians.nilai_max', 'ujians.jumlah_soal','ujians.waktu_pengerjaan','ujians.status','users.name', 'users.id as user_id','ujian_users.id', 'ujian_users.token','ujian_users.jawaban_benar','ujian_users.jawaban_salah','ujian_users.nilai')
        ->selectRaw('DATE_FORMAT(ujian_users.start_date, "%Y-%m-%d %H:%i:%s") as start_date')
        ->selectRaw('DATE_FORMAT(ujian_users.finish_date, "%Y-%m-%d %H:%i:%s") as finish_date')
        ->selectRaw('DATE_FORMAT(ujians.tgl_ujian, "%Y-%m-%d") as tgl_ujian')

        ->join('ujians', 'ujian_users.ujian_id', '=', 'ujians.id')
        ->join('users', 'ujian_users.user_id', '=', 'users.id')

        ->where('ujian_users.id','=', $id)
        ->whereNull('ujian_users.nilai')
        ->first();     
        
        //dd($query);
        if ($query === null) {
            return redirect('token')->with('success', 'Ujian tidak tersedia');
        }
        else{

            
            $queryTampilSoal           = DB::table('soal_users')
            ->select('soal_users.id as soal_users_id', 
            'soal_users.id_jawaban', 
            'soal_users.benar_salah',
            'soal_users.id_ujian_user',
            'soal_choices.soal_choice',
            'soal_choices.id as id_soal',)
            ->join('soal_choices', 'soal_users.id_soal', '=', 'soal_choices.id')
            ->where('soal_users.id_ujian_user','=', $id)
            ->orderBy('soal_users.id','asc')
            ->get();
            
            //dd($request);
            
            $totalJawabanSalah = 0;
            $totalJawabanBenar = 0;

            foreach($queryTampilSoal as $soal){

                $soalId             =   $soal->id_soal;
                if(isset( $_POST['jawaban_'."".$soalId])){                   
                    $ambilValue         =   $_POST['jawaban_'."".$soalId];
                }
                else{

                    $ambilValue         =   0;
                }

                $cekJawaban         =   DB::table('soal_choice_jawabans')
                ->select('id as id_jawaban')
                ->where('id_soal_choice','=', $soal->id_soal)
                ->where('status_jawaban','=', 1)
                ->first();

                if($ambilValue == $cekJawaban->id_jawaban){
                    $jawabanBenarSalah = '1';
                    $totalJawabanBenar++;
                }
                else{
                    $jawabanBenarSalah = '0';
                    $totalJawabanSalah++;
                }

                //dd( "ambilValue".$ambilValue." cekid_jawaban".$cekJawaban->id_jawaban." ".$_POST['jawaban_'."".$soalId]." --- ".$soalId);


                //dd($jawabanBenarSalah);

                $updateJawaban              = SoalUser::find($soal->soal_users_id);
                $updateJawaban->id_jawaban     = $ambilValue;
                $updateJawaban->benar_salah = $jawabanBenarSalah;
                $updateJawaban->save(); 

                $nilaiPerSoal   =   $query->nilai_max / $query->jumlah_soal;

                $totalNilai     =   $totalJawabanBenar * $nilaiPerSoal;
              
                $updateUjianUser                    =   UjianUser::find($soal->id_ujian_user);
                $updateUjianUser->jawaban_benar     =   $totalJawabanBenar;
                $updateUjianUser->jawaban_salah     =   $totalJawabanSalah;
                $updateUjianUser->finish_date       =   date('Y-m-d H:i:s');
                $updateUjianUser->nilai             =   $totalNilai ;
                $updateUjianUser->save(); 

            }

            

        }
        return redirect('ujian-selesai'.'/'.$id);
    }

    public function sertifikat(Request $request,$id)
    {
        
//dd( $request->getSchemeAndHttpHost());
        $query           =      DB::table('ujian_users')
        ->select('ujians.id as ujian_id', 'ujians.name', 'ujians.nilai_max', 'ujians.jumlah_soal','ujians.min_penuhi_syarat','ujians.waktu_pengerjaan','ujians.status','users.name', 'users.id as user_id','ujian_users.id', 'ujian_users.token','ujian_users.jawaban_benar','ujian_users.jawaban_salah','ujian_users.nilai',
        'users.name as userName',
        'users.alamat',
        'users.pekerjaan',
        'users.umur',
        
        )
        ->selectRaw('DATE_FORMAT(ujian_users.start_date, "%Y-%m-%d %H:%i:%s") as start_date')
        ->selectRaw('DATE_FORMAT(ujian_users.finish_date, "%Y-%m-%d %H:%i:%s") as finish_date')
        ->selectRaw('DATE_FORMAT(ujians.tgl_ujian, "%Y-%m-%d") as tgl_ujian')

        ->join('ujians', 'ujian_users.ujian_id', '=', 'ujians.id')
        ->join('users', 'ujian_users.user_id', '=', 'users.id')

        ->where('ujian_users.id','=', $id)
        ->whereNotNull('ujian_users.nilai')
        ->first();  

        if($query->nilai < $query->min_penuhi_syarat){
            $data['lulus'] = false;
            ///dd('sini');
        }
        else{
            $data['lulus'] = true;
        }
        
        $data['row']    =   $query;
        
        $string = url('/')."/show-sertifikat/".$query->id;
        $data['qrcode'] = base64_encode(QrCode::format('svg')->size(100)->errorCorrection('H')->generate($string));
  

        //dd( $data);
        $pdf = PDF::loadView('user.sertifikat', ['data' => $data]);
        
        
        return $pdf->download('sertifikat-smartpsi.pdf');
        
        //return view('user.sertifikat', $data);

    }

    public function showSertifikat(Request $request,$id)
    {
        
//dd( $request->getSchemeAndHttpHost());
        $query           =      DB::table('ujian_users')
        ->select('ujians.id as ujian_id', 'ujians.name', 'ujians.nilai_max', 'ujians.jumlah_soal','ujians.min_penuhi_syarat','ujians.waktu_pengerjaan','ujians.status','users.name', 'users.id as user_id','ujian_users.id', 'ujian_users.token','ujian_users.jawaban_benar','ujian_users.jawaban_salah','ujian_users.nilai',
        'users.name as userName',
        'users.alamat',
        'users.pekerjaan',
        'users.umur',
        
        )
        ->selectRaw('DATE_FORMAT(ujian_users.start_date, "%Y-%m-%d %H:%i:%s") as start_date')
        ->selectRaw('DATE_FORMAT(ujian_users.finish_date, "%Y-%m-%d %H:%i:%s") as finish_date')
        ->selectRaw('DATE_FORMAT(ujians.tgl_ujian, "%Y-%m-%d") as tgl_ujian')

        ->join('ujians', 'ujian_users.ujian_id', '=', 'ujians.id')
        ->join('users', 'ujian_users.user_id', '=', 'users.id')

        ->where('ujian_users.id','=', $id)
        ->whereNotNull('ujian_users.nilai')
        ->first();  

        if($query->nilai < $query->min_penuhi_syarat){
            $data['lulus'] = false;
            ///dd('sini');
        }
        else{
            $data['lulus'] = true;
        }
        
        $data['row']    =   $query;

        
        return view('user.showSertifikat', $data);

        

    }
}