<?php

namespace App\Http\Controllers;

use App\Models\Ujian;
use App\Models\UjianUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use DB;

class HasilUjianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function index(Request $request)
    {
        $data['title']  = 'Hasil Ujian ';
        $data['q']      = $request->q;
        $data['rows']   = Ujian::select('id', 'name', 'jumlah_soal','waktu_pengerjaan','status')
        ->selectRaw('DATE_FORMAT(tgl_ujian, "%d-%m-%Y") as tgl_ujian')->where('name', 'like', '%' . $request->q . '%')->paginate(10);
        return view('admin.hasil_ujian.index', $data);
    }
    public function show(Request $request, $id)
    {
        

        $post           = DB::table('ujian_users')
        ->select('ujians.id as ujian_id', 'ujians.name', 'ujians.jumlah_soal','ujians.waktu_pengerjaan','ujians.status','users.name', 'users.id as user_id','ujian_users.id', 'ujian_users.token','ujian_users.jawaban_benar','ujian_users.jawaban_salah','ujian_users.nilai')
        ->selectRaw('DATE_FORMAT(ujian_users.start_date, "%Y-%m-%d %H:%i") as start_date')
        ->selectRaw('DATE_FORMAT(ujian_users.finish_date, "%Y-%m-%d %H:%i") as finish_date')
        ->selectRaw('DATE_FORMAT(ujians.tgl_ujian, "%Y-%m-%d") as tgl_ujian')

        ->join('ujians', 'ujian_users.ujian_id', '=', 'ujians.id')
        ->join('users', 'ujian_users.user_id', '=', 'users.id')

        ->where('ujians.id','=',$id)
        ->Paginate(150);
        //dd($post);
        $data['rows']    = $post;

        $dataUjian           = DB::table('ujians')->select('id', 'name', 'jumlah_soal','waktu_pengerjaan','status')
        ->selectRaw('DATE_FORMAT(tgl_ujian, "%Y-%m-%d") as tgl_ujian')
        ->where('id','=',$id)
        ->first();

        $data['data_ujian']  = $dataUjian;

        $data['title']  = 'Data Peserta Ujian : '.$dataUjian->name;
        
        $data['q']      = $request->q;
        
        return view('admin.hasil_ujian.show', $data);
    }
    
}