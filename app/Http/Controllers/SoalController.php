<?php

namespace App\Http\Controllers;

use App\Models\SoalChoice;
use App\Models\SoalChoiceJawaban;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use DB;

class SoalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $data['title']  = 'Data Bank Soal ';
        $data['q']      = $request->q;
        $data['rows']   = SoalChoice::select('*')
        ->where('soal_choice', 'like', '%'.$request->q.'%')
        ->orderBy('id','desc')
        ->paginate(10);

            $data['htmlTable']  = "";
            $no = 1;
            foreach($data['rows'] as $row22){

                $data['htmlTable']  .= "
                <tr>
                    <td>". $row22['soal_choice']."</td>
                    <td>
                    <ul>
                ";
                    
                $data['rowsJawaban']   = SoalChoiceJawaban::select('*')
                ->where('id_soal_choice',$row22['id'])
                ->orderBy('id','asc')
                ->paginate(10);
                
                foreach($data['rowsJawaban'] as $row23){
                    if($row23['status_jawaban'] == 1){
                        $data['htmlTable']  .= "<li style='color:green;font-weight:bold'>".$row23['jawaban']."</li>";
                    }else{
                        $data['htmlTable']  .= "<li>".$row23['jawaban']."</li>";
                    }
                }

                    
                if( $row22->status_active == 1)
                   $statusActive = 'Aktif';
                else
                    $statusActive = 'Tidak Aktif';
                
                $data['htmlTable']  .= "
                    </ul>
                    </td>
                    <td>
                        ".$statusActive."
                    </td>
                    <td>
                        <a class='btn btn-sm btn-warning' href='". url('soal/edit', $row22['id'] ) ."'>Ubah</a>
                        <a class='btn btn-sm btn-danger' href='".url('soal/delete', $row22['id'])."'>Hapus</a>
                    </td>
                </tr>
                ";
            }

        return view('admin.soal_choice.index', $data);
    }

   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data['title'] = 'Tambah Bank Soal ';
        
        return view('admin.soal_choice.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'soal'          => 'required',
            'jawaban_1'    => 'required',
            'jawaban_2'     => 'required',
            'status_jawaban'     => 'required',
        ]);

        $soalInsert = SoalChoice::create([
            'soal_choice' => $request->soal
        ]);

        $newIdSoal = $soalInsert->id;

        if($request->jawaban_1 != ''){

            if($request->status_jawaban == 1){
                $statusJawab = 1;
            }
            else{
                $statusJawab = 0;
            }

            $soalInsert = SoalChoiceJawaban::create([
                'id_soal_choice'    => $newIdSoal,
                'jawaban'           => $request->jawaban_1,
                'status_jawaban'    => $statusJawab
            ]);
        }

        if($request->jawaban_2 != ''){

            if($request->status_jawaban == 2){
                $statusJawab = 1;
            }
            else{
                $statusJawab = 0;
            }

            $soalInsert = SoalChoiceJawaban::create([
                'id_soal_choice'    => $newIdSoal,
                'jawaban'           => $request->jawaban_2,
                'status_jawaban'    => $statusJawab
            ]);
        }

        if($request->jawaban_3!= ''){

            if($request->status_jawaban == 3){
                $statusJawab = 1;
            }
            else{
                $statusJawab = 0;
            }

            $soalInsert = SoalChoiceJawaban::create([
                'id_soal_choice'    => $newIdSoal,
                'jawaban'           => $request->jawaban_3,
                'status_jawaban'    => $statusJawab
            ]);
        }

        if($request->jawaban_4!= ''){

            if($request->status_jawaban == 4){
                $statusJawab = 1;
            }
            else{
                $statusJawab = 0;
            }

            $soalInsert = SoalChoiceJawaban::create([
                'id_soal_choice'    => $newIdSoal,
                'jawaban'           => $request->jawaban_4,
                'status_jawaban'    => $statusJawab
            ]);
        }

        
        if($request->jawaban_5!= ''){

            if($request->status_jawaban == 5){
                $statusJawab = 1;
            }
            else{
                $statusJawab = 0;
            }

            $soalInsert = SoalChoiceJawaban::create([
                'id_soal_choice'    => $newIdSoal,
                'jawaban'           => $request->jawaban_5,
                'status_jawaban'    => $statusJawab
            ]);
        }

        return redirect('soal')->with('success', 'Tambah Data Berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
   // public function edit(SoalChoice $soal)
    //{
    //    $data['title'] = 'Ubah Bank Soal ';
    public function edit($id)
    {
        $post           = SoalChoice::findOrFail($id);
        $data['row']    = $post;
        $data['status_active'] = ['1' => 'Aktif', '0' => 'Tidak Aktif'];
        $jawaban1s   = SoalChoiceJawaban::select('*')
        ->where('id_soal_choice',$id)
        ->orderBy('id')
        ->offset(0)
        ->limit(1)
        ->first();

        if($jawaban1s){
            $data['jawaban1']       = $jawaban1s['jawaban'];
            $data['jawaban1Benar']  = $jawaban1s['status_jawaban'];
        }
        else{
            $data['jawaban1']       = "";
            $data['jawaban1Benar']  = 0;
        }

        $jawaban2s   = SoalChoiceJawaban::select('*')
        ->where('id_soal_choice',$id)
        ->orderBy('id')        
        ->offset(1)
        ->limit(1)
        ->first();
        if($jawaban2s){
            $data['jawaban2']       =   $jawaban2s['jawaban'];
            $data['jawaban2Benar']  =   $jawaban2s['status_jawaban'];
        }
        else{
            $data['jawaban3']       = "";
            $data['jawaban2Benar']  = 0;
        }


        $jawaban3s   = SoalChoiceJawaban::select('*')
        ->where('id_soal_choice',$id)
        ->orderBy('id')   
        ->offset(2)
        ->limit(1)
        ->first();
        if($jawaban3s){
            $data['jawaban3']       =   $jawaban3s['jawaban'];
            $data['jawaban3Benar']  =   $jawaban3s['status_jawaban'];
        }
        else{
            $data['jawaban3']       = "";
            $data['jawaban3Benar']  = 0;
        }

        $jawaban4s   = SoalChoiceJawaban::select('*')
        ->where('id_soal_choice',$id)
        ->orderBy('id')
        ->offset(3)
        ->limit(1)
        ->first();
        if($jawaban4s){
            $data['jawaban4']       =   $jawaban4s['jawaban'];
            $data['jawaban4Benar']  =   $jawaban4s['status_jawaban'];
        }
        else{
            $data['jawaban4']       = "";
            $data['jawaban4Benar']  = 0;
        }

        $jawaban5s   = SoalChoiceJawaban::select('*')
        ->where('id_soal_choice',$id)
        ->orderBy('id')
        ->offset(4)
        ->limit(1)
        ->first();

        if($jawaban5s){
            $data['jawaban5']       =   $jawaban5s['jawaban'];
            $data['jawaban5Benar']  =   $jawaban5s['status_jawaban'];
        }
        else{
            $data['jawaban5']       = "";
            $data['jawaban5Benar']  = "23424";
        }
        
        //dd( $data['jawaban5Benar']);

        return view('admin.soal_choice.edit', $data);
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        DB::table('soal_choices')->where('id', $id)->update([
            'soal_choice' => $request->soal,
            'status_active' => $request->status_active,
        ]);

        DB::table('soal_choice_jawabans')->where('id_soal_choice', $id)->delete();
        
        if($request->jawaban_1 != ''){

            if($request->status_jawaban == 1){
                $statusJawab = 1;
            }
            else{
                $statusJawab = 0;
            }

            $soalInsert = SoalChoiceJawaban::create([
                'id_soal_choice'    => $id,
                'jawaban'           => $request->jawaban_1,
                'status_jawaban'    => $statusJawab
            ]);
        }

        if($request->jawaban_2 != ''){

            if($request->status_jawaban == 2){
                $statusJawab = 1;
            }
            else{
                $statusJawab = 0;
            }

            $soalInsert = SoalChoiceJawaban::create([
                'id_soal_choice'    => $id,
                'jawaban'           => $request->jawaban_2,
                'status_jawaban'    => $statusJawab
            ]);
        }

        if($request->jawaban_3!= ''){

            if($request->status_jawaban == 3){
                $statusJawab = 1;
            }
            else{
                $statusJawab = 0;
            }

            $soalInsert = SoalChoiceJawaban::create([
                'id_soal_choice'    => $id,
                'jawaban'           => $request->jawaban_3,
                'status_jawaban'    => $statusJawab
            ]);
        }

        if($request->jawaban_4!= ''){

            if($request->status_jawaban == 4){
                $statusJawab = 1;
            }
            else{
                $statusJawab = 0;
            }

            $soalInsert = SoalChoiceJawaban::create([
                'id_soal_choice'    => $id,
                'jawaban'           => $request->jawaban_4,
                'status_jawaban'    => $statusJawab
            ]);
        }

        
        if($request->jawaban_5!= ''){

            if($request->status_jawaban == 5){
                $statusJawab = 1;
            }
            else{
                $statusJawab = 0;
            }

            $soalInsert = SoalChoiceJawaban::create([
                'id_soal_choice'    => $id,
                'jawaban'           => $request->jawaban_5,
                'status_jawaban'    => $statusJawab
            ]);
        }

        return redirect('soal')->with('success', 'Ubah Data Berhasil');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(SoalChoice $id)
    {
        $id->delete();

        
        DB::table('soal_choice_jawabans')->where('id_soal_choice', $id)->delete();
        
        return redirect('soal')->with('success', 'Hapus Data Berhasil');
    }
    
}