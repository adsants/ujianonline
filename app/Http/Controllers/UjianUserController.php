<?php

namespace App\Http\Controllers;

use App\Models\UjianUser;
use App\Models\SoalUser;
use App\Exports\TokenUserExport;
use App\Exports\HasilUjianExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use DB;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Crypt;

class UjianUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function show(Request $request, $id)
    {
        

        $post           = DB::table('ujian_users')
        ->select('ujians.id as ujian_id', 'ujians.name as ujianName', 'ujians.jumlah_soal','ujians.waktu_pengerjaan','ujians.status','users.name', 'users.id as user_id','ujian_users.id', 'ujian_users.token','ujian_users.jawaban_benar','ujian_users.jawaban_salah','ujian_users.nilai')
        ->selectRaw('DATE_FORMAT(ujian_users.start_date, "%Y-%m-%d %H:%i") as start_date')
        ->selectRaw('DATE_FORMAT(ujian_users.finish_date, "%Y-%m-%d %H:%i") as finish_date')
        ->selectRaw('DATE_FORMAT(ujians.tgl_ujian, "%Y-%m-%d %H:%i") as tgl_ujian')

        ->join('ujians', 'ujian_users.ujian_id', '=', 'ujians.id')
        ->join('users', 'ujian_users.user_id', '=', 'users.id')

        ->where('ujians.id','=',$id)
        ->get();
        //dd($post);
        $data['rows']    = $post;

        $dataUjian           = DB::table('ujians')->select('id', 'name', 'jumlah_soal','waktu_pengerjaan','status')
        ->selectRaw('DATE_FORMAT(tgl_ujian, "%Y-%m-%d") as tgl_ujian')
        ->where('id','=',$id)
        ->first();

        $data['data_ujian']  = $dataUjian;

        $data['title']  = 'Data Peserta Ujian : '.$dataUjian->name;
        
        $data['q']      = $request->q;
        
        return view('admin.ujian_user.show', $data);
    }

    public function create(Request $request, $id)
    {
        
        $dataUjian           = DB::table('ujians')->select('id', 'name', 'jumlah_soal','waktu_pengerjaan','status')
        ->selectRaw('DATE_FORMAT(tgl_ujian, "%Y-%m-%d") as tgl_ujian')
        ->where('id','=',$id)
        ->first();
        $data['data_ujian']     = $dataUjian;
        
        
        return view('admin.ujian_user.create', $data);
    }
    public function store(Request $request, $id)
    {
        
        $dataUjian           = DB::table('ujians')
        ->select('id', 'name', 'jumlah_soal','waktu_pengerjaan','status')
        ->selectRaw('DATE_FORMAT(tgl_ujian, "%Y-%m-%d") as tgl_ujian')
        ->where('id','=',$id)
        ->first();

        $request->validate([
            'ujian_id' => 'required',
            'user_id' => 'required'
        ]);

        
        $variable   =   $id."-".$request->user_id;
        $encrypted  =   Crypt::encryptString($variable);
        
        
        $input              = new UjianUser();
        $input->ujian_id    = $request->ujian_id;
        $input->user_id     = $request->user_id;
        $input->token       = $encrypted;
        $input->save();
        $ujianUserIdNew     = $input->id;


        $dataUjian           = DB::table('ujians')
        ->select('id','jenis_soal','jumlah_soal')
        ->where('id','=',$request->ujian_id)
        ->first();

        $dataSoals           = DB::table('soal_choices')
        ->select('id')
        ->inRandomOrder()
        ->limit($dataUjian->jumlah_soal)
        ->get();



        foreach($dataSoals as $dataSoal){
            $inputSoalUjianUser                 = new SoalUser();
            $inputSoalUjianUser->id_soal        = $dataSoal->id;
            $inputSoalUjianUser->id_ujian_user  = $ujianUserIdNew;
            $inputSoalUjianUser->save();
        }


        return redirect('ujian-user/show/'. $dataUjian->id)->with('success', 'Tambah Data Berhasil');
    }
    public function destroy($id){

        $deleteSoalUsers         = DB::table('soal_users')
        ->join('ujian_users', 'ujian_users.id', '=', 'soal_users.id_ujian_user')
        ->where('ujian_users.id','=',$id)
        ->delete();

        $ujianUser = UjianUser::find($id);
        $ujianUser ->delete();

        return redirect('ujian-user/show/'.$ujianUser->ujian_id)->with('success', 'Hapus Data Berhasil');
    }

    public function exportToken($id) 
    {
        $data = DB::table('ujian_users')
        ->select('ujians.id as ujian_id', 'ujians.name as ujian_name', 'ujians.jumlah_soal','ujians.waktu_pengerjaan','ujians.status','users.name', 'users.id as user_id','ujian_users.id', 'ujian_users.token','ujian_users.jawaban_benar','ujian_users.jawaban_salah','ujian_users.nilai')
        ->selectRaw('DATE_FORMAT(ujian_users.start_date, "%Y-%m-%d %H:%i") as start_date')
        ->selectRaw('DATE_FORMAT(ujian_users.finish_date, "%Y-%m-%d %H:%i") as finish_date')
        ->selectRaw('DATE_FORMAT(ujians.tgl_ujian, "%Y-%m-%d") as tgl_ujian')

        ->join('ujians', 'ujian_users.ujian_id', '=', 'ujians.id')
        ->join('users', 'ujian_users.user_id', '=', 'users.id')

        ->where('ujians.id','=',$id)
        ->first();

        return Excel::download(new TokenUserExport($id), 'Token User - Ujian - '.$data->ujian_name.'.xlsx');
    }

    public function exportHasilUjian($id) 
    {
        $data = DB::table('ujian_users')
        ->select('ujians.id as ujian_id', 'ujians.name as ujian_name', 'ujians.jumlah_soal','ujians.waktu_pengerjaan','ujians.status','users.name', 'users.id as user_id','ujian_users.id', 'ujian_users.token','ujian_users.jawaban_benar','ujian_users.jawaban_salah','ujian_users.nilai')
        ->selectRaw('DATE_FORMAT(ujian_users.start_date, "%Y-%m-%d %H:%i") as start_date')
        ->selectRaw('DATE_FORMAT(ujian_users.finish_date, "%Y-%m-%d %H:%i") as finish_date')
        ->selectRaw('DATE_FORMAT(ujians.tgl_ujian, "%Y-%m-%d") as tgl_ujian')

        ->join('ujians', 'ujian_users.ujian_id', '=', 'ujians.id')
        ->join('users', 'ujian_users.user_id', '=', 'users.id')

        ->where('ujians.id','=',$id)
        ->first();

        return Excel::download(new HasilUjianExport($id), 'Hasil Ujian - '.$data->ujian_name.'.xlsx');
    }
     

    
}