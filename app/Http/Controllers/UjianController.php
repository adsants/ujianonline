<?php

namespace App\Http\Controllers;

use App\Models\Ujian;
use App\Models\UjianUser;
use App\Models\SoalUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use DB;
use Illuminate\Support\Facades\Crypt;

class UjianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['title']  = 'Data Ujian ';
        $data['q']      = $request->q;
        $data['rows']   = Ujian::select('id', 'name','min_penuhi_syarat','nilai_max','token', 'jenis_soal', 'jumlah_soal','waktu_pengerjaan','status')
        ->selectRaw('DATE_FORMAT(tgl_ujian, "%d-%m-%Y %H:%i") as tgl_ujian')->where('name', 'like', '%' . $request->q . '%')
        ->orderBy('id','desc')
        ->paginate(10);
        return view('admin.ujian.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $data['title'] = 'Tambah Ujian ';
        
        $data['status'] = ['1' => 'Aktif', '0' => 'Tidak Aktif'];
        return view('admin.ujian.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $stringToken = rand(1111, 9999);

        $request->validate([
            'name' => 'required',
            'tgl_ujian' => 'required',
            'jumlah_soal' => 'required',
            'waktu_pengerjaan' => 'required',
            'nilai_max' => 'required',
            'status' => 'required',
            'min_penuhi_syarat' => 'required',
        ]);

        $input = new Ujian();
        $input->name                = $request->name;
        $input->token               = $stringToken;
        $input->tgl_ujian           = $request->tgl_ujian;
        $input->nilai_max           = $request->nilai_max;
        $input->jenis_soal          = $request->jenis_soal;
        $input->jumlah_soal         = $request->jumlah_soal;
        $input->waktu_pengerjaan    = $request->waktu_pengerjaan;
        $input->status              = $request->status;
        $input->min_penuhi_syarat              = $request->min_penuhi_syarat;

        $input->save();
        $ujianIdNew = $input->id;

        $users           = DB::table('users')->select('id')
        ->where('role','=','user')
        ->get();


        $i=1;
        foreach($users as $user){

            //$variable   =   $ujianIdNew."-".$user->id;
            //$encrypted  =   Crypt::encryptString($variable);
            //$decrypted  =   Crypt::decryptString($encrypted);
    
            $token      =   time()."".$user->id;

            $inputUjian             = new UjianUser();
            $inputUjian->ujian_id   = $ujianIdNew;
            $inputUjian->user_id    = $user->id;
            $inputUjian->token      = $token;
            $inputUjian->kurang_waktu_pengerjaan      = $request->waktu_pengerjaan;
            $inputUjian->save();

            $ujianUserIdNew         = $inputUjian->id;            

            $dataSoals              = DB::table('soal_choices')
            ->select('id')
            ->groupBy('id')
            ->inRandomOrder()
            ->limit($request->jumlah_soal)
            ->get();
            
           // dd( $dataSoals );
            
            foreach($dataSoals as $dataSoal){
                $inputSoalUjianUser                 = new SoalUser();
                $inputSoalUjianUser->id_soal        = $dataSoal->id;
                $inputSoalUjianUser->id_ujian_user  = $ujianUserIdNew;
                $inputSoalUjianUser->save();
            }

            $i++;
        }
   
        
    
        return redirect('ujian')->with('success', 'Tambah Data Berhasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
   // public function edit(Ujian $soal)
    //{
    //    $data['title'] = 'Ubah Ujian ';
    public function edit($id)
    {
        //$post           = DB::table('ujians')->where('id','=',$id)->first();

        $post           = DB::table('ujians')
        ->select('id', 'token', 'jenis_soal','name', 'jumlah_soal','waktu_pengerjaan','status','nilai_max','min_penuhi_syarat')
        ->selectRaw('DATE_FORMAT(tgl_ujian, "%Y-%m-%d %H:%i:%s") as tgl_ujian')
        ->where('id','=',$id)
        ->first();

        //dd($post->tgl_ujian);
        
        $data['row']    = $post;
        $data['status'] = ['1' => 'Aktif', '0' => 'Tidak Aktif'];
        return view('admin.ujian.edit', $data);
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model          = Ujian::find($id);
        $model->name    = $request->name;
        $model->tgl_ujian   = $request->tgl_ujian;
        $model->nilai_max    = $request->nilai_max;
        $model->min_penuhi_syarat  = $request->min_penuhi_syarat;
        $model->jumlah_soal    = $request->jumlah_soal;
        $model->waktu_pengerjaan     = $request->waktu_pengerjaan;
        $model->status     = $request->status;
        $model->save();
        
        return redirect('ujian')->with('success', 'Ubah Data Berhasil');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ujian $id)
    {
        $deleteSoalUsers         = DB::table('soal_users')
        ->join('ujian_users', 'ujian_users.id', '=', 'soal_users.id_ujian_user')
        ->where('ujian_users.ujian_id','=',$id->id)
        ->delete();

        $deleteUjianUsers          = DB::table('ujian_users')
        ->where('ujian_id','=',$id->id)
        ->delete();

        $id->delete();

        return redirect('ujian')->with('success', 'Hapus Data Berhasil');
    }
    
}