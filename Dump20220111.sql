-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: ujian
-- ------------------------------------------------------
-- Server version	5.7.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_admins`
--

DROP TABLE IF EXISTS `menu_admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menu_admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL,
  `menu` varchar(45) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_admins`
--

LOCK TABLES `menu_admins` WRITE;
/*!40000 ALTER TABLE `menu_admins` DISABLE KEYS */;
INSERT INTO `menu_admins` VALUES (14,1,'user','2021-10-01 23:37:37','2021-10-01 23:37:37'),(15,1,'soal','2021-10-01 23:37:37','2021-10-01 23:37:37'),(16,1,'ujian','2021-10-01 23:37:37','2021-10-01 23:37:37'),(17,1,'hasil_ujian','2021-10-01 23:37:37','2021-10-01 23:37:37'),(18,1,'grafik','2021-10-01 23:37:37','2021-10-01 23:37:37'),(19,34,'user','2021-10-02 02:42:58','2021-10-02 02:42:58');
/*!40000 ALTER TABLE `menu_admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2019_12_14_000001_create_personal_access_tokens_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `soal_angka_hilangs`
--

DROP TABLE IF EXISTS `soal_angka_hilangs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `soal_angka_hilangs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `data_soal` varchar(255) NOT NULL,
  `jawaban_benar` varchar(45) NOT NULL,
  `jenis_soal` varchar(45) NOT NULL COMMENT 'angka, huruf, simbol, kombinasi',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=313 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `soal_angka_hilangs`
--

LOCK TABLES `soal_angka_hilangs` WRITE;
/*!40000 ALTER TABLE `soal_angka_hilangs` DISABLE KEYS */;
INSERT INTO `soal_angka_hilangs` VALUES (1,'4,5,6,7,8','8','Angka','2021-09-30 08:55:55','2021-09-30 08:55:55'),(2,'a,b,c,d,e','e','Huruf','2021-09-30 08:57:45','2021-09-30 08:57:45'),(3,'!,@,#,%,^','^','Simbol','2021-09-30 08:58:01','2021-09-30 08:58:01'),(4,'1,A,@,$,6','6','Kombinasi','2021-09-30 08:58:48','2021-09-30 08:58:48'),(6,'1,2,3,4,5','5','Angka','2021-09-30 16:24:30','2021-09-30 16:24:30'),(8,'^,%,&,(,*','*','Simbol','2021-09-30 16:25:39','2021-09-30 16:25:39'),(9,'9,8,7,6,5','5','Angka','2021-09-30 16:59:16','2021-09-30 16:59:16'),(10,'{,},|,-,+','+','Simbol','2021-10-01 02:46:05','2021-10-01 02:46:05'),(11,'^,%,&,(,*','*','Huruf','2021-10-21 17:59:05','2021-10-21 17:59:05'),(12,'Ჵ,∆,Ს,Ⴟ,Დ','Დ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(13,'∆,Ჵ,Დ,Ს,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(14,'Დ,Ⴟ,∆,Ჵ,Ს','Ს','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(15,'Ს,Დ,Ⴟ,∆,Ჵ','Ჵ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(16,'Ⴟ,Ს,Ჵ,Დ,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(17,'Ⴟ,Დ,Ჵ,Ს,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(18,'∆,Ჵ,Ⴟ,Დ,Ს','Ს','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(19,'Დ,Ს,∆,Ჵ,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(20,'Ს,∆,Დ,Ⴟ,Ჵ','Ჵ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(21,'Ჵ,Ⴟ,Ს,∆,Დ','Დ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(22,'Ს,Ჵ,Დ,∆,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(23,'Ⴟ,Დ,Ს,Ჵ,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(24,'Ჵ,∆,Ⴟ,Დ,Ს','Ს','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(25,'Დ,Ს,∆,Ⴟ,Ჵ','Ჵ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(26,'∆,Ⴟ,Ჵ,Ს,Დ','Დ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(27,'∆,Დ,Ს,Ⴟ,Ჵ','Ჵ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(28,'Დ,Ჵ,Ⴟ,Ს,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(29,'Ს,∆,Ჵ,Დ,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(30,'Ჵ,Ⴟ,Დ,∆,Ს','Ს','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(31,'Ⴟ,Ს,∆,Ჵ,Დ','Დ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(32,'Ს,∆,Ჵ,Ⴟ,Დ','Დ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(33,'∆,Დ,Ს,Ჵ,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(34,'Ⴟ,Ჵ,Დ,∆,Ს','Ს','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(35,'Დ,Ⴟ,∆,Ს,Ჵ','Ჵ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(36,'Ჵ,Ს,Ⴟ,Დ,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(37,'Ჵ,Დ,Ს,Ⴟ,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(38,'Ს,Ⴟ,Დ,∆,Ჵ','Ჵ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(39,'Დ,∆,Ჵ,Ს,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(40,'Ⴟ,Ჵ,∆,Დ,Ს','Ს','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(41,'∆,Ს,Ⴟ,Ჵ,Დ','Დ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(42,'Ⴟ,∆,Ჵ,Დ,Ს','Ს','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(43,'Ჵ,Დ,∆,Ს,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(44,'∆,Ჵ,Ს,Ⴟ,Დ','Დ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(45,'Ს,Ⴟ,Დ,Ჵ,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(46,'Დ,Ს,Ⴟ,∆,Ჵ','Ჵ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(47,'Დ,Ჵ,Ს,Ⴟ,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(48,'Ⴟ,Ს,∆,Დ,Ჵ','Ჵ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(49,'Ჵ,∆,Დ,Ს,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(50,'Ს,Ⴟ,Ჵ,∆,Დ','Დ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(51,'∆,Დ,Ⴟ,Ჵ,Ს','Ს','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(52,'Ს,∆,Დ,Ⴟ,Ჵ','Ჵ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(53,'∆,Ჵ,Ⴟ,Დ,Ს','Ს','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(54,'Დ,Ს,Ჵ,∆,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(55,'Ჵ,Ⴟ,∆,Ს,Დ','Დ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(56,'Ⴟ,Დ,Ს,Ჵ,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(57,'∆,Ⴟ,Ჵ,Ს,Დ','Დ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(58,'Დ,Ს,∆,Ⴟ,Ჵ','Ჵ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(59,'Ⴟ,∆,Დ,Ჵ,Ს','Ს','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(60,'Ჵ,Დ,Ს,∆,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(61,'Ს,Ჵ,Ⴟ,Დ,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(62,'Ღ,⌂,Ჵ,Დ,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(63,'Ჵ,Დ,Ღ,Ჵ,⌂','⌂','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(64,'⌂,Ჵ,Დ,∆,Ღ','Ღ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(65,'Დ,∆,⌂,Ღ,Ჵ','Ჵ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(66,'∆,Ღ,∆,⌂,Დ','Დ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(67,'Ჵ,Დ,Ღ,⌂,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(68,'∆,⌂,Ჵ,Დ,Ღ','Ღ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(69,'⌂,Ჵ,∆,Ღ,Დ','Დ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(70,'Ღ,∆,Დ,Ჵ,⌂','⌂','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(71,'Დ,Ღ,⌂,∆,Ჵ','Ჵ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(72,'∆,Ღ,Ჵ,Დ,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(73,'Ჵ,⌂,Დ,∆,Ღ','Ღ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(74,'Ღ,Დ,⌂,Ჵ,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(75,'Დ,∆,Ღ,⌂,Ჵ','Ჵ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(76,'⌂,Ჵ,∆,Ღ,Დ','Დ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(77,'Დ,⌂,∆,Ჵ,Ღ','Ღ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(78,'Ღ,Დ,⌂,∆,Ჵ','Ჵ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(79,'∆,Ჵ,Ღ,Დ,⌂','⌂','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(80,'Ჵ,Ღ,Დ,⌂,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(81,'⌂,∆,Ჵ,Ღ,Დ','Დ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(82,'⌂,Ღ,∆,Ჵ,Დ','Დ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(83,'Ჵ,∆,⌂,Დ,Ღ','Ღ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(84,'∆,Ჵ,Დ,Ღ,⌂','⌂','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(85,'Ღ,Დ,Ჵ,⌂,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(86,'Დ,⌂,Ღ,∆,Ჵ','Ჵ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(87,'Დ,Ჵ,Ღ,⌂,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(88,'Ღ,Დ,⌂,∆,Ჵ','Ჵ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(89,'∆,⌂,Დ,Ჵ,Ღ','Ღ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(90,'Ჵ,Ღ,∆,Დ,⌂','⌂','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(91,'⌂,∆,Ჵ,Ღ,Დ','Დ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(92,'Ღ,⌂,∆,Დ,Ჵ','Ჵ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(93,'∆,Ღ,Დ,Ჵ,⌂','⌂','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(94,'Ჵ,Დ,Ღ,⌂,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(95,'⌂,∆,Ჵ,Ღ,Დ','Დ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(96,'Დ,Ჵ,⌂,∆,Ღ','Ღ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(97,'Დ,Ღ,Ჵ,⌂,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(98,'Ჵ,Დ,Ღ,∆,⌂','⌂','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(99,'⌂,∆,Დ,Ჵ,Ღ','Ღ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(100,'Ღ,⌂,∆,Დ,Ჵ','Ჵ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(101,'∆,Ჵ,⌂,Ღ,Დ','Დ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(102,'∆,Დ,Ჵ,Ღ,⌂','⌂','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(103,'⌂,Ღ,Დ,Ჵ,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(104,'Ღ,∆,⌂,Დ,Ჵ','Ჵ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(105,'Ჵ,⌂,Ღ,∆,Დ','Დ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(106,'Დ,Ჵ,∆,⌂,Ღ','Ღ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(107,'Ღ,∆,⌂,Დ,Ჵ','Ჵ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(108,'Დ,Ღ,Ჵ,∆,⌂','⌂','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(109,'∆,⌂,Ღ,Ჵ,Დ','Დ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(110,'⌂,Ჵ,Დ,Ღ,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(111,'Ჵ,Დ,∆,⌂,Ღ','Ღ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(112,'Ⴔ,Թ,Ⴟ,∆,Ს','Ს','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(113,'∆,Ⴟ,Թ,Ს,Ⴔ','Ⴔ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(114,'Ს,∆,Ⴔ,Ⴟ,Թ','Թ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(115,'Թ,Ს,∆,Ⴔ,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(116,'Ⴟ,Ⴔ,Ს,Թ,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(117,'Ს,Ⴔ,Ⴟ,Թ,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(118,'∆,Թ,Ⴔ,Ს,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(119,'Թ,Ს,∆,Ⴟ,Ⴔ','Ⴔ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(120,'Ⴟ,∆,Ს,Ⴔ,Թ','Թ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(121,'Ⴔ,Ⴟ,Թ,∆,Ს','Ს','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(122,'Ⴔ,Թ,Ⴟ,Ს,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(123,'Ⴟ,Ⴔ,Ს,∆,Թ','Թ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(124,'∆,Ს,Թ,Ⴟ,Ⴔ','Ⴔ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(125,'Թ,Ⴟ,∆,Ⴔ,Ს','Ს','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(126,'Ს,∆,Ⴔ,Թ,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(127,'Թ,∆,Ⴟ,Ს,Ⴔ','Ⴔ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(128,'Ⴔ,Թ,Ს,Ⴟ,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(129,'Ს,Ⴟ,∆,Ⴔ,Թ','Թ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(130,'∆,Ს,Ⴔ,Թ,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(131,'Ⴟ,Ⴔ,Թ,∆,Ს','Ს','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(132,'Ს,Ⴟ,∆,Թ,Ⴔ','Ⴔ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(133,'Թ,∆,Ს,Ⴔ,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(134,'Ⴔ,Թ,Ⴟ,∆,Ს','Ს','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(135,'∆,Ს,Ⴔ,Ⴟ,Թ','Թ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(136,'Ⴟ,Ⴔ,Թ,Ს,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(137,'Ⴟ,Ⴔ,Թ,∆,Ს','Ს','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(138,'Թ,∆,Ⴔ,Ს,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(139,'Ⴔ,Ⴟ,Ს,Թ,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(140,'Ს,Թ,∆,Ⴟ,Ⴔ','Ⴔ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(141,'∆,Ს,Ⴟ,Ⴔ,Թ','Թ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(142,'∆,Ⴟ,Ს,Ⴔ,Թ','Թ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(143,'Ⴟ,Թ,Ⴔ,Ს,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(144,'Թ,Ს,Ⴟ,∆,Ⴔ','Ⴔ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(145,'Ს,Ⴔ,∆,Թ,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(146,'Ⴔ,∆,Թ,Ⴟ,Ს','Ს','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(147,'Ⴔ,∆,Թ,Ს,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(148,'Ⴟ,Ⴔ,Ს,∆,Թ','Թ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(149,'Թ,Ⴟ,∆,Ⴔ,Ს','Ს','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(150,'∆,Թ,Ⴟ,Ს,Ⴔ','Ⴔ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(151,'Ს,Ⴟ,Ⴔ,Թ,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(152,'Ⴟ,∆,Ს,Թ,Ⴔ','Ⴔ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(153,'Ს,Թ,Ⴔ,∆,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(154,'Թ,Ⴔ,Ⴟ,Ს,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(155,'∆,Ⴟ,Թ,Ⴔ,Ს','Ს','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(156,'Ⴔ,Ს,∆,Ⴟ,Թ','Թ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(157,'∆,Ს,Ⴟ,Ⴔ,Թ','Թ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(158,'Ⴟ,Ⴔ,Թ,∆,Ს','Ს','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(159,'Ს,Թ,∆,Ⴟ,Ⴔ','Ⴔ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(160,'Ⴔ,∆,Ს,Թ,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(161,'Թ,Ⴟ,Ⴔ,Ს,∆','∆','Simbol','2021-10-21 18:03:24','2021-10-21 18:03:24'),(162,'Ჵ,∆,Ს,Ⴟ,Დ','Დ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(163,'∆,Ჵ,Დ,Ს,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(164,'Დ,Ⴟ,∆,Ჵ,Ს','Ს','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(165,'Ს,Დ,Ⴟ,∆,Ჵ','Ჵ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(166,'Ⴟ,Ს,Ჵ,Დ,∆','∆','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(167,'Ⴟ,Დ,Ჵ,Ს,∆','∆','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(168,'∆,Ჵ,Ⴟ,Დ,Ს','Ს','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(169,'Დ,Ს,∆,Ჵ,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(170,'Ს,∆,Დ,Ⴟ,Ჵ','Ჵ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(171,'Ჵ,Ⴟ,Ს,∆,Დ','Დ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(172,'Ს,Ჵ,Დ,∆,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(173,'Ⴟ,Დ,Ს,Ჵ,∆','∆','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(174,'Ჵ,∆,Ⴟ,Დ,Ს','Ს','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(175,'Დ,Ს,∆,Ⴟ,Ჵ','Ჵ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(176,'∆,Ⴟ,Ჵ,Ს,Დ','Დ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(177,'∆,Დ,Ს,Ⴟ,Ჵ','Ჵ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(178,'Დ,Ჵ,Ⴟ,Ს,∆','∆','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(179,'Ს,∆,Ჵ,Დ,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(180,'Ჵ,Ⴟ,Დ,∆,Ს','Ს','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(181,'Ⴟ,Ს,∆,Ჵ,Დ','Დ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(182,'Ს,∆,Ჵ,Ⴟ,Დ','Დ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(183,'∆,Დ,Ს,Ჵ,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(184,'Ⴟ,Ჵ,Დ,∆,Ს','Ს','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(185,'Დ,Ⴟ,∆,Ს,Ჵ','Ჵ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(186,'Ჵ,Ს,Ⴟ,Დ,∆','∆','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(187,'Ჵ,Დ,Ს,Ⴟ,∆','∆','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(188,'Ს,Ⴟ,Დ,∆,Ჵ','Ჵ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(189,'Დ,∆,Ჵ,Ს,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(190,'Ⴟ,Ჵ,∆,Დ,Ს','Ს','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(191,'∆,Ს,Ⴟ,Ჵ,Დ','Დ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(192,'Ⴟ,∆,Ჵ,Დ,Ს','Ს','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(193,'Ჵ,Დ,∆,Ს,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(194,'∆,Ჵ,Ს,Ⴟ,Დ','Დ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(195,'Ს,Ⴟ,Დ,Ჵ,∆','∆','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(196,'Დ,Ს,Ⴟ,∆,Ჵ','Ჵ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(197,'Დ,Ჵ,Ს,Ⴟ,∆','∆','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(198,'Ⴟ,Ს,∆,Დ,Ჵ','Ჵ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(199,'Ჵ,∆,Დ,Ს,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(200,'Ს,Ⴟ,Ჵ,∆,Დ','Დ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(201,'∆,Დ,Ⴟ,Ჵ,Ს','Ს','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(202,'Ს,∆,Დ,Ⴟ,Ჵ','Ჵ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(203,'∆,Ჵ,Ⴟ,Დ,Ს','Ს','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(204,'Დ,Ს,Ჵ,∆,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(205,'Ჵ,Ⴟ,∆,Ს,Დ','Დ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(206,'Ⴟ,Დ,Ს,Ჵ,∆','∆','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(207,'∆,Ⴟ,Ჵ,Ს,Დ','Დ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(208,'Დ,Ს,∆,Ⴟ,Ჵ','Ჵ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(209,'Ⴟ,∆,Დ,Ჵ,Ს','Ს','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(210,'Ჵ,Დ,Ს,∆,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(211,'Ს,Ჵ,Ⴟ,Დ,∆','∆','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(212,'Ღ,⌂,Ჵ,Დ,∆','∆','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(213,'Ჵ,Დ,Ღ,Ჵ,⌂','⌂','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(214,'⌂,Ჵ,Დ,∆,Ღ','Ღ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(215,'Დ,∆,⌂,Ღ,Ჵ','Ჵ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(216,'∆,Ღ,∆,⌂,Დ','Დ','Simbol','2021-10-21 18:03:44','2021-10-21 18:03:44'),(217,'Ჵ,Დ,Ღ,⌂,∆','∆','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(218,'∆,⌂,Ჵ,Დ,Ღ','Ღ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(219,'⌂,Ჵ,∆,Ღ,Დ','Დ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(220,'Ღ,∆,Დ,Ჵ,⌂','⌂','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(221,'Დ,Ღ,⌂,∆,Ჵ','Ჵ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(222,'∆,Ღ,Ჵ,Დ,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(223,'Ჵ,⌂,Დ,∆,Ღ','Ღ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(224,'Ღ,Დ,⌂,Ჵ,∆','∆','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(225,'Დ,∆,Ღ,⌂,Ჵ','Ჵ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(226,'⌂,Ჵ,∆,Ღ,Დ','Დ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(227,'Დ,⌂,∆,Ჵ,Ღ','Ღ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(228,'Ღ,Დ,⌂,∆,Ჵ','Ჵ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(229,'∆,Ჵ,Ღ,Დ,⌂','⌂','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(230,'Ჵ,Ღ,Დ,⌂,∆','∆','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(231,'⌂,∆,Ჵ,Ღ,Დ','Დ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(232,'⌂,Ღ,∆,Ჵ,Დ','Დ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(233,'Ჵ,∆,⌂,Დ,Ღ','Ღ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(234,'∆,Ჵ,Დ,Ღ,⌂','⌂','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(235,'Ღ,Დ,Ჵ,⌂,∆','∆','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(236,'Დ,⌂,Ღ,∆,Ჵ','Ჵ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(237,'Დ,Ჵ,Ღ,⌂,∆','∆','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(238,'Ღ,Დ,⌂,∆,Ჵ','Ჵ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(239,'∆,⌂,Დ,Ჵ,Ღ','Ღ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(240,'Ჵ,Ღ,∆,Დ,⌂','⌂','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(241,'⌂,∆,Ჵ,Ღ,Დ','Დ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(242,'Ღ,⌂,∆,Დ,Ჵ','Ჵ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(243,'∆,Ღ,Დ,Ჵ,⌂','⌂','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(244,'Ჵ,Დ,Ღ,⌂,∆','∆','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(245,'⌂,∆,Ჵ,Ღ,Დ','Დ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(246,'Დ,Ჵ,⌂,∆,Ღ','Ღ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(247,'Დ,Ღ,Ჵ,⌂,∆','∆','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(248,'Ჵ,Დ,Ღ,∆,⌂','⌂','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(249,'⌂,∆,Დ,Ჵ,Ღ','Ღ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(250,'Ღ,⌂,∆,Დ,Ჵ','Ჵ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(251,'∆,Ჵ,⌂,Ღ,Დ','Დ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(252,'∆,Დ,Ჵ,Ღ,⌂','⌂','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(253,'⌂,Ღ,Დ,Ჵ,∆','∆','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(254,'Ღ,∆,⌂,Დ,Ჵ','Ჵ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(255,'Ჵ,⌂,Ღ,∆,Დ','Დ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(256,'Დ,Ჵ,∆,⌂,Ღ','Ღ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(257,'Ღ,∆,⌂,Დ,Ჵ','Ჵ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(258,'Დ,Ღ,Ჵ,∆,⌂','⌂','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(259,'∆,⌂,Ღ,Ჵ,Დ','Დ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(260,'⌂,Ჵ,Დ,Ღ,∆','∆','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(261,'Ჵ,Დ,∆,⌂,Ღ','Ღ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(262,'Ⴔ,Թ,Ⴟ,∆,Ს','Ს','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(263,'∆,Ⴟ,Թ,Ს,Ⴔ','Ⴔ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(264,'Ს,∆,Ⴔ,Ⴟ,Թ','Թ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(265,'Թ,Ს,∆,Ⴔ,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(266,'Ⴟ,Ⴔ,Ს,Թ,∆','∆','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(267,'Ს,Ⴔ,Ⴟ,Թ,∆','∆','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(268,'∆,Թ,Ⴔ,Ს,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(269,'Թ,Ს,∆,Ⴟ,Ⴔ','Ⴔ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(270,'Ⴟ,∆,Ს,Ⴔ,Թ','Թ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(271,'Ⴔ,Ⴟ,Թ,∆,Ს','Ს','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(272,'Ⴔ,Թ,Ⴟ,Ს,∆','∆','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(273,'Ⴟ,Ⴔ,Ს,∆,Թ','Թ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(274,'∆,Ს,Թ,Ⴟ,Ⴔ','Ⴔ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(275,'Թ,Ⴟ,∆,Ⴔ,Ს','Ს','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(276,'Ს,∆,Ⴔ,Թ,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(277,'Թ,∆,Ⴟ,Ს,Ⴔ','Ⴔ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(278,'Ⴔ,Թ,Ს,Ⴟ,∆','∆','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(279,'Ს,Ⴟ,∆,Ⴔ,Թ','Թ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(280,'∆,Ს,Ⴔ,Թ,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(281,'Ⴟ,Ⴔ,Թ,∆,Ს','Ს','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(282,'Ს,Ⴟ,∆,Թ,Ⴔ','Ⴔ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(283,'Թ,∆,Ს,Ⴔ,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(284,'Ⴔ,Թ,Ⴟ,∆,Ს','Ს','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(285,'∆,Ს,Ⴔ,Ⴟ,Թ','Թ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(286,'Ⴟ,Ⴔ,Թ,Ს,∆','∆','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(287,'Ⴟ,Ⴔ,Թ,∆,Ს','Ს','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(288,'Թ,∆,Ⴔ,Ს,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(289,'Ⴔ,Ⴟ,Ს,Թ,∆','∆','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(290,'Ს,Թ,∆,Ⴟ,Ⴔ','Ⴔ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(291,'∆,Ს,Ⴟ,Ⴔ,Թ','Թ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(292,'∆,Ⴟ,Ს,Ⴔ,Թ','Թ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(293,'Ⴟ,Թ,Ⴔ,Ს,∆','∆','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(294,'Թ,Ს,Ⴟ,∆,Ⴔ','Ⴔ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(295,'Ს,Ⴔ,∆,Թ,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(296,'Ⴔ,∆,Թ,Ⴟ,Ს','Ს','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(297,'Ⴔ,∆,Թ,Ს,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(298,'Ⴟ,Ⴔ,Ს,∆,Թ','Թ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(299,'Թ,Ⴟ,∆,Ⴔ,Ს','Ს','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(300,'∆,Թ,Ⴟ,Ს,Ⴔ','Ⴔ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(301,'Ს,Ⴟ,Ⴔ,Թ,∆','∆','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(302,'Ⴟ,∆,Ს,Թ,Ⴔ','Ⴔ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(303,'Ს,Թ,Ⴔ,∆,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(304,'Թ,Ⴔ,Ⴟ,Ს,∆','∆','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(305,'∆,Ⴟ,Թ,Ⴔ,Ს','Ს','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(306,'Ⴔ,Ს,∆,Ⴟ,Թ','Թ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(307,'∆,Ს,Ⴟ,Ⴔ,Թ','Թ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(308,'Ⴟ,Ⴔ,Թ,∆,Ს','Ს','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(309,'Ს,Թ,∆,Ⴟ,Ⴔ','Ⴔ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(310,'Ⴔ,∆,Ს,Թ,Ⴟ','Ⴟ','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(311,'Թ,Ⴟ,Ⴔ,Ს,∆','∆','Simbol','2021-10-21 18:03:45','2021-10-21 18:03:45'),(312,'1,2,3,4,5','5','Angka','2021-10-23 07:53:03','2021-10-23 07:53:03');
/*!40000 ALTER TABLE `soal_angka_hilangs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `soal_choice_jawabans`
--

DROP TABLE IF EXISTS `soal_choice_jawabans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `soal_choice_jawabans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_soal_choice` int(11) NOT NULL,
  `jawaban` longblob NOT NULL,
  `status_jawaban` int(11) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `soal_choice_jawabans`
--

LOCK TABLES `soal_choice_jawabans` WRITE;
/*!40000 ALTER TABLE `soal_choice_jawabans` DISABLE KEYS */;
INSERT INTO `soal_choice_jawabans` VALUES (1,3,_binary '<p>jawabn 1</p>',0,'2022-01-06 09:25:04','2022-01-06 09:25:04'),(2,3,_binary '<p>jawab 2</p>',0,'2022-01-06 09:25:04','2022-01-06 09:25:04'),(3,3,_binary '<p>jawab 3</p>',0,'2022-01-06 09:25:04','2022-01-06 09:25:04'),(4,3,_binary '<p>Jawab Benar</p>',1,'2022-01-06 09:25:04','2022-01-06 09:25:04'),(14,4,_binary '<p>jawabn 1</p>',0,'2022-01-11 03:32:21','2022-01-11 03:32:21'),(15,4,_binary '<p>jawab 2</p>',0,'2022-01-11 03:32:21','2022-01-11 03:32:21'),(16,4,_binary '<p>jawab 3</p>',0,'2022-01-11 03:32:21','2022-01-11 03:32:21'),(17,4,_binary '<p>Jawab Benar 4</p>',1,'2022-01-11 03:32:21','2022-01-11 03:32:21'),(18,4,_binary '<p>Jwaban Salah 5</p>',0,'2022-01-11 03:32:21','2022-01-11 03:32:21'),(19,5,_binary '<p>A</p>',0,'2022-01-11 03:34:57','2022-01-11 03:34:57'),(20,5,_binary '<p>xc</p>',1,'2022-01-11 03:34:57','2022-01-11 03:34:57'),(21,5,_binary '<p>B</p>',0,'2022-01-11 03:34:57','2022-01-11 03:34:57'),(22,5,_binary '<p>C</p>',0,'2022-01-11 03:34:57','2022-01-11 03:34:57'),(23,5,_binary '<p>R</p>',0,'2022-01-11 03:34:57','2022-01-11 03:34:57');
/*!40000 ALTER TABLE `soal_choice_jawabans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `soal_choices`
--

DROP TABLE IF EXISTS `soal_choices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `soal_choices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `soal_choice` longblob NOT NULL,
  `jumlah_jawaban` int(11) DEFAULT NULL,
  `status_active` int(11) DEFAULT '1',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `soal_choices`
--

LOCK TABLES `soal_choices` WRITE;
/*!40000 ALTER TABLE `soal_choices` DISABLE KEYS */;
INSERT INTO `soal_choices` VALUES (4,_binary '<p>soal</p>',NULL,0,'2022-01-06 09:25:28','2022-01-06 09:25:28');
/*!40000 ALTER TABLE `soal_choices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `soal_gambars`
--

DROP TABLE IF EXISTS `soal_gambars`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `soal_gambars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_soal` int(11) NOT NULL,
  `soal` longblob NOT NULL,
  `status` enum('B','S') DEFAULT 'S',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `soal_gambars`
--

LOCK TABLES `soal_gambars` WRITE;
/*!40000 ALTER TABLE `soal_gambars` DISABLE KEYS */;
INSERT INTO `soal_gambars` VALUES (1,1635059522,_binary '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAfCAYAAACVgY94AAAAuElEQVRYhe3VQQrDIBAFUG/oZfQo8RziNcRTTNbuZ/W7SCkxhdrYNg50hL8wiLw4jBoIH2Y2oDdMrRWSY5gZkqNABX4lOcBae09AEQVcE7xLIGYwE5KzsI+5ACDFgLQewNa33+YCfVtWacCn5CCrxG0IyUlrkl3K8n5px4DNddHJoYwUPUI+/1PngUvpr2uulg3nI+32ST9qkgHg1sWvT3f6CX4SBSpQgf8HHHxJrgFOiAIVODvigTfkhIN+INxgjwAAAABJRU5ErkJggg==\" /></p>','B','2021-10-26 02:14:16','2021-10-26 02:14:16'),(2,1635059522,_binary '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAjCAYAAAAXMhMjAAAA0ElEQVRYhe3YvQ2DMBCGYTa8ip4KZYJbJbo9fHNY3sI0rPClIIlEohB8imUruldyQcWD/woGdNzQGnDUsK4reh19z1xrwFGOs/YjXIbyiOkyY/44JowkSG1w316cII57y3HWHGfNcda6voTrdBqXA4OIQM+vT5Ddc0PcVoYyQWL5/rFUvKzbDNaHAZY9tyiYFbkC5rViXAoCYYYuNTj7inA5MCQC6UqQCGBRaKwkw0nc46RyuC9mlO3kVl7e/7jnWuQ4a46z5jhrfeNa/4M7GjdM6NFO6RnP6AAAAABJRU5ErkJggg==\" /></p>','S','2021-10-26 02:14:16','2021-10-26 02:14:16'),(3,1635059522,_binary '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACcAAAAiCAYAAADcbsCGAAAA10lEQVRYhe2YwQ3DIAxFs6GXwaPAHJHXiDyFOeee0++tUhMaUak1qDLSvxgEj28wEsu+75hVy3EcmFUBF3ABF3BD4aqAiL6mr8JpLtApnauCstqMaTVIurqmuT+F3Lmxz+G2grK10kzN+Fm28q/gDJIF1ugbDmcrvwVwgDNIIlDWxmBFacZd4AySGLIJmBhSz5OWS2xIWjWf3KsCvnHN98zVV/d6Cq7rhdBMoCSwzoLre1urgIlA1PdMuZeS3gWfTg9/IRwVcAEXcAH393Cj/+Bu/+cwcXsAhHarseQLQ0wAAAAASUVORK5CYII=\" /></p>','S','2021-10-26 02:14:16','2021-10-26 02:14:16'),(4,1635059522,_binary '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACgAAAAhCAYAAACr8emlAAAA4klEQVRYhe2VOw7EIAxEc0NfBo6CzxH5GpFPYer0qWa7lTZxdlG0fAqQpkEIPcb2sGDwtez7jpG1HMeBkTUBJ2BvnQAVTAS6iKGjAMpql0O2ygSsA5jFaYfn+jugpvq9+XxIsoCdx1QGLJVBgg8di0ta5v4zwI3Bm9+TMQjs5x0KrgdokHQDMQKgrdF3rx2gQQKBkvoXu/vNAA0SImQTRIqQfHaPL3vdSqzp5GIWxG/uNe/B/OliUSi3HhJNBAoCKw3l5lP8Dt7CL61HzGii+1i5fVDvn6ShJuAE7K0JOAF76wW5gXtoGPKZZAAAAABJRU5ErkJggg==\" /></p>','S','2021-10-26 02:14:16','2021-10-26 02:14:16'),(5,1635059522,_binary '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAgCAYAAAB6kdqOAAAAbUlEQVRYhe3VsQ2AMAxE0WzobVgFZQ8YJFtQeQVnAUsHheGI7kuu8wrLae4eTNOCLIFQAqHWAV3HFmaWTI/xDehMHx67QALdBZEtdVUCoR6D8r3J5xVQdQKhBEKtc6kJ/zKBfgciW+qqBELRgSYNG4xIIH6cJQAAAABJRU5ErkJggg==\" /></p>','S','2021-10-26 02:14:16','2021-10-26 02:14:16'),(6,1635090193,_binary '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAAgCAYAAADqgqNBAAAA0UlEQVRIie2VsRXDMAhEvSHLSKOYOfxYQ48pUO1e1aWIky6xsHlWo+JavuAOtOz7jlFaWmsYpQmf8AkfBK+CTAS6oLzZ/c5tyyDKkNpTxCDpeEAS2P2xHwVdxRTc/eBTzxVMBFq1f5RVwI7R/w9cYRARuPR3Y0W7p3Wadl0JRAx1eBnT+WX/w+Dtu37eVYqBt8/6+fwPg7fCvuTHwRU8xnODpEFp19V3tcLgtmVfwKogO+z5DS/sXC2DpIjbXgXZkez3FYz41Ub/509pwif8Ub0AZni44KfepfAAAAAASUVORK5CYII=\" /></p>','S','2021-10-26 02:14:16','2021-10-26 02:14:16'),(7,1635090193,_binary '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAfCAYAAABplKSyAAAA4ElEQVRYhe3VwQ2DMAwFUDb0Nl4FZYpe4jkib5FcWOH3ENFCiEBqGwdRLHFBkXiK7c+AE9TQGwDciHd9jNCRQLR9XGiJCO71IfYR6gWxPJME0gyRBEwOCiB6BpHDwxoRPYNGXb0zvokI4dyCjgiFqwyc8WDWEb+sY0QSMDEk9UQEB+qNmFdSjw5eHqEjgbiyjn+GyEFVpmUXRJmWxogcVKdAtEzLY0SRlnldt/+Lb29qHxHcKiOil0peKKQlInperacRYjmI23kwu4ll38ted2nHHtBuMI3qRsw1TNOE3s8T3fhlB0aOPZUAAAAASUVORK5CYII=\" /></p>','S','2021-10-26 02:14:16','2021-10-26 02:14:16'),(8,1635090193,_binary '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACMAAAAeCAYAAACmPacqAAAA10lEQVRYhe3WwQ2EIBAFUDucbqYVQh9Qh6ELuNDC34OXjTuDqOzumPATLkbwjSC4wFCWfwPeMzFaJkbLUzAZkT3SoAclx4jlIqan87kcFydicmBwyMqABKK+5tdd9xLBHCGNrGASPGkVZETue2PJCZjGdRmzepCqv4/JgUFOLvUDkxypN4/AtIoVMfJ6GYQpEawsgx1mW6Dfx8hjWMaYmiZjC9jUp3286d3YgRtIBdM6Dm7m/HGwJblxJ3bvmI1fiNZ0XYHo09OB+X0mRsvEaLGFqbXCSnsBZIfC+sXx5MAAAAAASUVORK5CYII=\" /></p>','S','2021-10-26 02:14:16','2021-10-26 02:14:16'),(9,1635090193,_binary '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB8AAAAfCAYAAAAfrhY5AAAAqUlEQVRIie3UvQ3DIBAFYDZ8jXtXVhYwo/jmsFgDMQWp6aleCkuxkfJXGJCVQ3oFDR8HB4Ydh0kpsVdMzpm9ovgV8Eg3DxxvE6e3GTlAGOrg3xYOFMUVV/zieLdP5tx8xMMCAlvsGlvhgXIEvRCzY6yPR7oZxBLKEzjM6+FeCFi6+34CNap+icfVPu9530SrhvNCFE9lq158CzyXXY4f3+xpeKsorvj/4A8at0qkTRkOHQAAAABJRU5ErkJggg==\" /></p>','S','2021-10-26 02:14:16','2021-10-26 02:14:16'),(10,1635090193,_binary '<p><img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACEAAAAfCAYAAABplKSyAAABMElEQVRYhe2WPW6FMBCEueFcxnsU+wRJj/Ya1kq5g6npqSaFHw9kGrAfP4qCtJIFzaeZYT3dNE28e7q7AT4HMSgFIBBotyoRA+HtXjvMgyFebkeiOhAAQzSGSivqIQalQKjDokKtFZUQWYG19OZB6dOFEJsAGsNKlUsgygCaB+GUqTpbjRDmQXHSZEWdHe/FlGHML+frIE6Yf4hDEHhl4Mj8TSWeCxFDlr5xUTUrkfr2RdUIkaiu7c6ohDAGgIBQo1Kc8qcXAsstmnrJ3w/A7YeIYdUh83X+tmLIQFZpz06I8rouO0UBdQbEJoCDUoo6l3o5s2OWAXx1y6LYqJMz2/YaIp9lU+8CbZrLbqLFY7bsy8S8mCDUYW7Zwu8vWXWJ5f3R37Ybx5F3T8cHPI+A+AUpzRXieb82RAAAAABJRU5ErkJggg==\" /></p>','B','2021-10-26 02:14:16','2021-10-26 02:14:16');
/*!40000 ALTER TABLE `soal_gambars` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `soal_users`
--

DROP TABLE IF EXISTS `soal_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `soal_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_soal` int(11) NOT NULL,
  `id_ujian_user` int(11) NOT NULL,
  `jawaban` varchar(45) DEFAULT NULL,
  `benar_salah` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `soal_users`
--

LOCK TABLES `soal_users` WRITE;
/*!40000 ALTER TABLE `soal_users` DISABLE KEYS */;
INSERT INTO `soal_users` VALUES (1,1635059522,1,NULL,NULL,'2021-10-25 17:19:59','2021-10-25 17:19:59'),(2,1635090193,1,NULL,NULL,'2021-10-25 17:19:59','2021-10-25 17:19:59'),(3,1635059522,2,NULL,NULL,'2021-10-25 17:19:59','2021-10-25 17:19:59'),(4,1635090193,2,NULL,NULL,'2021-10-25 17:19:59','2021-10-25 17:19:59'),(5,1635090193,3,'6',0,'2021-10-25 17:19:59','2021-10-26 00:31:19'),(6,1635059522,3,'2',0,'2021-10-25 17:19:59','2021-10-26 00:31:19'),(7,1635059522,4,'1',1,'2021-10-25 17:19:59','2021-10-26 00:34:20'),(8,1635090193,4,'10',1,'2021-10-25 17:19:59','2021-10-26 00:34:20'),(9,1635059522,5,NULL,NULL,'2021-10-25 17:19:59','2021-10-25 17:19:59'),(10,1635090193,5,NULL,NULL,'2021-10-25 17:19:59','2021-10-25 17:19:59');
/*!40000 ALTER TABLE `soal_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ujian_users`
--

DROP TABLE IF EXISTS `ujian_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ujian_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ujian_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(10) DEFAULT NULL,
  `jawaban_benar` int(11) DEFAULT NULL,
  `jawaban_salah` int(11) DEFAULT NULL,
  `nilai` decimal(6,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `finish_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ujian_users`
--

LOCK TABLES `ujian_users` WRITE;
/*!40000 ALTER TABLE `ujian_users` DISABLE KEYS */;
INSERT INTO `ujian_users` VALUES (1,1,30,NULL,NULL,NULL,NULL,'2021-10-25 17:19:59','2021-10-25 17:19:59',NULL,NULL),(2,1,31,NULL,NULL,NULL,NULL,'2021-10-25 17:19:59','2021-10-25 17:19:59',NULL,NULL),(3,1,32,NULL,0,2,0.00,'2021-10-25 17:19:59','2021-10-26 00:31:19','2021-10-26 07:31:12','2021-10-26 07:31:19'),(4,1,33,NULL,2,0,72.00,'2021-10-25 17:19:59','2021-10-26 00:34:20','2021-10-26 07:34:00','2021-10-26 07:34:20'),(5,1,36,NULL,NULL,NULL,NULL,'2021-10-25 17:19:59','2021-10-25 17:19:59',NULL,NULL);
/*!40000 ALTER TABLE `ujian_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ujians`
--

DROP TABLE IF EXISTS `ujians`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ujians` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `tgl_ujian` datetime NOT NULL,
  `jumlah_soal` int(11) NOT NULL,
  `waktu_pengerjaan` int(11) NOT NULL,
  `jenis_soal` varchar(45) NOT NULL,
  `token` varchar(45) NOT NULL,
  `nilai_max` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ujians`
--

LOCK TABLES `ujians` WRITE;
/*!40000 ALTER TABLE `ujians` DISABLE KEYS */;
INSERT INTO `ujians` VALUES (1,'Ujian Gambar','2021-10-27 00:00:00',2,25,'Gambar','1677',72,1,'2021-10-25 17:19:59','2021-10-25 17:19:59');
/*!40000 ALTER TABLE `ujians` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin','admin',NULL,'$2y$10$qLp4IAob1ze5OzjVZF9GAuqvIJXmm2Ukoh/3lJSaiAzqcMkLmyOn2',NULL,'admin','2021-09-24 06:54:50','2021-10-01 23:37:37'),(30,'Siswa Satu','satu',NULL,'$2y$10$MIzvGcf8bgOEpcPYCuAmPOAgGLIpc3Jk84og1BAETAibhHiN/3t6q',NULL,'user','2021-10-01 23:38:43','2021-10-01 23:38:43'),(31,'Siswa Dua','dua',NULL,'$2y$10$yfqDE58lsjfBhW5laNRp4u1/SL6B.gKiH/zlSRCXjda9Wm4vaptRS',NULL,'user','2021-10-01 23:38:43','2021-10-01 23:38:43'),(32,'Siswa Tiga','tiga',NULL,'$2y$10$E6OqW9sCJXWHUxY9Dq7GF.W1dwv5KQ97ynTZKV0FWwaKF4elql8vO',NULL,'user','2021-10-01 23:38:43','2021-10-01 23:38:43'),(33,'Peserta Empat','empat',NULL,'$2y$10$6Anfr3ZjQaLjPwJ8DGnvZeC7cPIZErHs494jlAE1HqKqrIw/TDlnK',NULL,'user','2021-10-01 23:41:52','2021-10-01 23:41:52'),(34,'admin2','admin2',NULL,'$2y$10$Ijq5Ybix7g82ZYdn7EjI1uC5ZBxF9HPPEgbvggh5BU6TueoiWx46C',NULL,'admin','2021-10-02 02:42:58','2021-10-02 02:42:58'),(35,'Admin','admin3',NULL,'$2y$10$qLp4IAob1ze5OzjVZF9GAuqvIJXmm2Ukoh/3lJSaiAzqcMkLmyOn2',NULL,'admin','2021-10-20 13:03:20','2021-10-20 13:03:20'),(36,'Lima','lima',NULL,'$2y$10$QXtDmmIVPve8d/5rdYGapuCF3HBlP0XHbraRLCUQ5zXv8vL3XC4Lq',NULL,'user','2021-10-20 17:31:43','2021-10-20 17:31:43');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-11  7:41:55
