@extends('../../layouts.app')

@section('content')


@if(session('success'))
<p class="alert alert-success">{{ session('success') }}</p>
@endif

<style>
    img{
        max-width: 100%;
        height: auto;
    }
</style>
<div class="card card-default">
    
    <div class="card-header">
        Data Soal BerGambar
    </div>
    <div class="card-body p-4">
        <div class="row">
            <div class="col-md-6">
                @if($errors->any())
                @foreach($errors->all() as $err)
                <p class="alert alert-danger">{{ $err }}</p>
                @endforeach
                @endif
                <form >
                    <div class="form-group">
                        <label>Type Soal <span class="text-danger">*</span></label>
                        <select class="form-control" required name="type_soal" id="type_soal" onchange="gantiSoal()">
                            <option value="">Silahkan Pilih</option>
                            <option value="noGambar">Soal Non gambar</option>
                            <option selected value="gambar">Soal Bergambar</option>
                        </select>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="card-header">
        <form class="form-inline">
            <div class="form-group mr-1">
                <input class="form-control" type="text" name="q" value="{{ $q}}" placeholder="Pencarian... Enter" />
            </div>
            <div class="form-group mr-1">
                <a class="btn btn-primary" href="{{ url('soal/gambar') }}">Refresh</a>
            </div>
            <div class="form-group mr-1">
                <a class="btn btn-primary" href="{{ url('soal/create-gambar') }}">Tambah</a>
            </div>
            <div class="form-group mr-1 text-right">
                <a class="btn btn-success" href="{{ url('soal/json-file-download') }}">Export</a>
                &nbsp;
                <a class="btn btn-warning" href="{{ url('soal/import-gambar') }}">Import</a>
            </div>
        </form>
    </div>
    <div class="card-body p-0 table-responsive">
        {!! $tableData !!}
        <br>
        <br>
        <div class="d-flex justify-content-center">
        {{ $pagianate->links() }}
    </div>
    </div>
</div>


<script>
    function gantiSoal(){
        if($('#type_soal').val() == 'gambar'){
            location.href="{{URL::to('soal/gambar')}}";
        }
        else if($('#type_soal').val() == 'noGambar'){            
            location.href="{{URL::to('soal/nogambar')}}";
        }
        else{

        }
    }
</script>
@endsection