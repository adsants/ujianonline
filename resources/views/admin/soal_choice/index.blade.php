@extends('../../layouts.app')

@section('content')


@if(session('success'))
<p class="alert alert-success">{{ session('success') }}</p>
@endif
<div class="card card-default">
    
    <div class="card-header">
        Data Soal
    </div>
    <div class="card-header">
        <form class="form-inline">
            <div class="form-group mr-1">
                <input class="form-control" type="text" name="q" value="{{ $q}}" placeholder="Pencarian... Enter" />
            </div>
            <div class="form-group mr-1">
                <a class="btn btn-primary" href="{{ url('soal') }}">Refresh</a>
            </div>
            <div class="form-group mr-1">
                <a class="btn btn-primary" href="{{ url('soal/create') }}">Tambah</a>
            </div>
            <div class="form-group mr-1 text-right">
                <!--<a class="btn btn-success" href="{{ url('export-soal') }}">Export</a>
                &nbsp; -->
                <a class="btn btn-warning" href="{{ url('import-soal-view') }}">Import</a>
            </div>
        </form>
    </div>
    <div class="card-body p-0 table-responsive">
        <table class="table table-bordered table-striped table-hover mb-0">
            <thead>
                <tr>
                    <th>Soal</th>
                    <th>Jawaban</th>
                    <th>Status Aktif</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            {!! $htmlTable !!}
        </table>
        <br>
        <br>
        <div class="d-flex justify-content-center">
        {{ $rows->links() }}
    </div>
    </div>
</div>


@endsection