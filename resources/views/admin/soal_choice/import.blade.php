@extends('../../layouts.app')

@section('content')

<div class="card card-default">
    <div class="card-header">
        Import Data Soal
    </div>
    <div class="card-body p-4">
        <div class="row">
            <div class="col-md-8">
                @if($errors->any())
                @foreach($errors->all() as $err)
                <p class="alert alert-danger">{{ $err }}</p>
                @endforeach
                @endif
                <form action="{{ route('import-soal') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    Melalui Form ini hanya dapat Import Soal yang tidak bergambar (Text dan Angka).
                    <br>
                    <br>
                    <input type="file" name="file" class="form-control">
                     
                        <a target="_BLANK" href="{{ asset('public/documents/soal-choice.xlsx') }}">contoh file excel</a>
                    <br>
                    <br>
                    <button class="btn btn-success">Import Soal Data</button>
                    <a class="btn btn-danger" href="{{ url('soal') }}">Kembali</a>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection