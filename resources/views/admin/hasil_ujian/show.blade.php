@extends('../../layouts.app')

@section('content')


@if(session('success'))
<p class="alert alert-success">{{ session('success') }}</p>
@endif


<div class="card card-default">
    <div class="card-header">
        <h4>{{$title}}</h4>
    </div>
    <div class="card-header">
        <form class="form-inline">
            
            <div class="form-group mr-1">
                <a class="btn btn-primary" href="{{ url('hasil-ujian')  }}">Kembali</a>
            </div>
            
            <div class="form-group mr-1 text-right">
                <a class="btn btn-success" href="{{ url('export-hasil-ujian').'/'.$data_ujian->id }}">Export</a>
            </div>
        </form>
    </div>
    <div class="card-body p-0 table-responsive">
        <table class="table table-bordered table-striped table-hover mb-0">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Tgl Pengerjaan</th>
                    <th>Jumlah Benar</th>
                    <th>Jumlah Salah</th>
                    <th>Nilai</th>
                </tr>
            </thead>
            <?php $no = 1 ?>
            @foreach($rows as $row)
            <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $row->name }}</td>
                <td>{{ $row->start_date }} s/d {{ $row->finish_date }}</td>
                <td>{{ $row->jawaban_benar }}</td>
                <td>{{ $row->jawaban_salah }}</td>
                <td>{{ $row->nilai }}</td>
            </tr>
            @endforeach
        </table>
        <br>
        <br>
        <div class="d-flex justify-content-center">
        {{ $rows->links() }}
    </div>
    </div>
</div>
@endsection