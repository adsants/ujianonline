@extends('layouts.app')
 
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Informasi Ujian</div>
 
                <div class="card-body">
                <div class="bd-content ps-lg-4">
        
                    <div class="row g-3">

                        <div class="col-md-4">
                            <a class="d-block text-decoration-none" href="#">
                            <span class="text-secondary">Nama</span>
                            <strong class="d-block h5 mb-0">{{$row->name}}</strong>
                            </a>
                        </div>
                        <div class="col-md-4">
                            <a class="d-block text-decoration-none" href="#">
                            <span class="text-secondary">Nama Ujian</span>
                            <strong class="d-block h5 mb-0">{{$row->ujian_name}}</strong>
                            </a>
                        </div>

                        <div class="col-md-2">
                            <a class="d-block text-decoration-none" href="#">
                            <span class="text-secondary">Jumlah Soal</span>
                            <strong class="d-block h5 mb-0">{{$row->jumlah_soal}}</strong>
                            </a>
                        </div>

                        <div class="col-md-2">
                            <a class="d-block text-decoration-none" href="#">
                            <span class="text-secondary">Waktu Pengerjaan</span>
                            <strong class="d-block h5 mb-0">{{$row->waktu_pengerjaan}} Menit</strong>
                            </a>
                        </div>

                        
                        <div class="col-md-4 mt-4">
                            <a class="d-block text-decoration-none" href="#">
                            <span class="text-secondary">Tanngal Pengerjaan Ujian</span>
                            <strong class="d-block h5 mb-0">{{$row->tgl_ujian_indo}}</strong>
                            </a>
                        </div>
                    </div>
                    <br>
                    
                    <p>Klik Tombol Mulai untuk memulai mengerjakan Ujian. Bersamaan dengan itu, maka perhitungan waktu pengerjaan Ujian akan dimulai.</p>

                
                    <a href="{{ url('ujian-mulai', $row->id ) }}">
                        <span class="btn btn-lg btn-success">Mulai</span>
                    </a>
                        

                </div>
            </div>
        </div>
    </div>
</div>
@endsection